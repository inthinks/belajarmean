var express        = require('express');                          
var app            = express();               
var http           = require("http").Server(app); //servernya
var mongoose = require("mongoose"); //orm
var bodyParser = require('body-parser'); //aplikasi yg diterima hanya berupa Json 

app.use(bodyParser.json());  //use body parser Json
mongoose.connect('mongodb://localhost/mongos'); //koneksi ke mongo db 'mongos' nama db.

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true })); //urlEncoded accept aplication 
http.listen('3000'); //port yang kosong
app.get('/',function(req,res){ //index
    res.send('hello world');
});
var Barang = mongoose.model('barang', { //orm model=barang / create table barang
                namabarang : {type : String}, //property /create field otomatis
                stok : {type:String}, //field
                status : {type : String, default: ''}
            });

app.get("/data/api",function(req,res){ //menampilkan data dari model barang
    Barang.find().exec().then(function(docs){ // 
        res.json(docs); //parameter ini digunakan untuk menampung datanya
    })
});

app.post("/data/api",function(req, res){
    var Barangbaru = new Barang();
            Barangbaru.namabarang = req.body.namabarang; //body = request dari client dengan name namabarang
            Barangbaru.stok = req.body.stok;
            Barangbaru.status = req.body.status;

                Barangbaru.save(function(){
                    res.end()
                })
})

app.put("/data/api", function(req, res){
    var namabarang = req.body.namabarang;
    var stok = req.body.stok;
    var status = req.body.status;
    var id = req.body.id;
    Barang.update({_id:id}, {$set:
        {
            namabarang:namabarang,
            stok:stok,
            status:status
        }
    }, function(){
         res.end();
    })          
})          
